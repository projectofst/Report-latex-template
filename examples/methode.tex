\documentclass[a4paper]{article}
\usepackage[dvipsnames]{color}

\usepackage{graphicx}
\usepackage{FSTReport}

\usepackage{hyperref}
\graphicspath{{./graphics/}}

\usepackage{siunitx}
\sisetup{inter-unit-product =$\cdot$}

% Tables
\usepackage{booktabs}


\begin{document}

\begin{titlepage}

	\includegraphics[width=5cm]{ist.eps}
	\hfill
	\includegraphics[width=5cm]{ulisboa.eps}
    \hspace{3mm}
	\vspace{2cm}
	\begin{center}
		\includegraphics[width=6cm]{Logo-FST-Lisboa.eps}
	\end{center}
	\color{FSTRed}\rule{\textwidth}{2pt}
	\color{black}
	\begin{center}
		{\Huge\textbf{\Title}}\\ %% <-- DOC TITLE

		\vfill
	\Large \Author\\
		\vspace{1cm}
	\Large \today\\
		\vspace{1cm}
	\end{center}

\end{titlepage}

\section{Introduction to the solution}

Our 4th electric car features a high voltage battery with the following specifications:
\begin{itemize}
\item Composed of 288 LiCO2 cells in 144s2p configuration.
\item Voltage: 533 V nominal, 600 V maximum.
\item Current: 225 A maximum continuous discharge, 270 A peak discharge. 30 A continuous charge, 60 A peak charge.
\item Temperature: \SI{60}{\celsius} maximum.
\end{itemize}

\begin{figure}[H]
	\center
	\includegraphics[width=\textwidth]{battery-exploded}
	\caption{Exploded view of the complete battery}
	\label{fig:battery-exploded}
\end{figure}

The container of the battery is made of aluminum and welded at the joints.

\begin{figure}[H]
	\center
	\includegraphics[width=\textwidth]{accumulator-container}
	\caption{Accumulator container}
	\label{fig:closeup-socket-solder-top}
\end{figure}

The container features 7 sections. 6 of them hold the 288 cells and the 7th one is used for electronics. The sectioning of the battery makes working on the battery safer.

Inside each of the compartments there is a fire resistant carbon fiber box in which two cell stacks are hold.

\begin{figure}[H]
	\center
	\includegraphics[width=\textwidth]{stack-box}
	\caption{Stack box}
	\label{fig:stack-box}
\end{figure}

Each stack holds 24 cells in 12s2p configuration and a PCB (part of the BMS) responsible for monitoring voltage and temperature of the individual cells and balancing them.

\begin{figure}[H]
	\center
	\includegraphics[trim={5cm 0 0 0},clip,width=\textwidth]{two-stacks}
	\caption{Cell stacks with mounted PCB}
	\label{fig:two-stacks}
\end{figure}

The cells are electrically connected with using aluminum bus bars to which they are laser welded.

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth]{cell-welding}
	\caption{Closeup of the cell pads welded to the busbars with the Methode pins visible}
	\label{fig:cell-welding}
\end{figure}

In the middle of each busbar there is pin from Methode which connects to sockets on the BMS PCB. This connections serve both as connections to the cell's poles and as mechanical support to the PCB.

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth]{PCB-mounted}
	\caption{Closeup of the connection between the busbars and the PCB}
	\label{fig:PCB-mounted}
\end{figure}

\section{Methode Embedded Bud Connector usage}
The socket used is the 1101-06638-01104 with the corresponding pin 9104-06644-02104. This connectors are designed for bus bar connections up to 120 A by press fitting them to the bus bars.

Our usage is quite different from the connector's intended purpose.

The connectors are not part of the high current path. They are used as voltage measuring connections, as current path for cell balancing (max 300 mA) and as fixation of the PCB to the cell stack.

In this application we are interested in small size, 100\% contact reliability and low contact resistance between busbar and PCB.

Unfortunately only the pin was correctly press fitted to the bus bar while the socket had to be soldered to the PCB.

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth]{closeup-socket-solder-top}
	\caption{Socket solder on the top layer}
	\label{fig:closeup-socket-solder-top}
\end{figure}

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth]{closeup-socket-solder-bot}
	\caption{Socket solder on the bottom layer}
	\label{fig:closeup-socket-solder-bot}
\end{figure}

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth]{closeup-pin-pressfit-top}
	\caption{Pin press fitted (from above)}
	\label{fig:closeup-pin-pressfit-top}
\end{figure}

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth]{closeup-pin-pressfit-side}
	\caption{Pin press fitted (from the side)}
	\label{fig:closeup-pin-pressfit-side}
\end{figure}

This had to be done because when we tried to press fit the females into the PCB it cracked. This was the result of some holes being placed very close to the edge of the board. Slightly bigger hole diameter and more board space should solve this problem. As a side note the pins are highly solderable.


\section{Solution Life}
The battery was used during all the testing of the car. This comprised about thermal 35 cycles where the battery went from ambient temperature to \SI{55}{\celsius} in about half an hour and then cooled down to ambient temperature in about 2 to 3 hours. During the thermal cycles the whole battery was also submitted to vibration.

\section{Reliability}
During the tests both outside of the car and within the car there wasn't a single registered problem with the voltage connections, making the reliability and the confidence in the contacts 100\%.


\section{Contact resistance measurements}
Contact resistance measurements were taken from a sample of contacts. The measurements showed very low contact resistance (less than \SI{100}{\micro\ohm}) and the whole assembly (busbar to pin press fit contact, pin body, pin to socket contact, socket body and socket to PCB soldered connection) measured less than \SI{2}{\milli\ohm}.
Contact resistance measurements were also taken at different temperatures (between \SI{30}{\celsius} and \SI{80}{\celsius}). Results are shown in figure \ref{graph:resistance-vs-temperature}. The tests showed no correlation between temperature and press fitted contact resistance and that the press-fitted contact resistance is always lower than \SI{100}{\micro\ohm}.

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth, angle=-90,origin=c]{methode-pin-test-hoven}
	\caption{Oven used to heat the busbars}
	\label{fig:methode-pin-test-hoven}
\end{figure}

\begin{figure}[H]
	\center
	\includegraphics[width=0.9\textwidth]{resistance-vs-temperature}
	\caption{Graph of mean contact resistance of contacts batch vs temperature}
	\label{graph:resistance-vs-temperature}
\end{figure}

\end{document}

