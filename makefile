FILE=FSTReport
make:
	pdflatex $(FILE).tex
	pdflatex $(FILE).tex

#############################################################
# Maintenance

# compress with ghostscript
gs:
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(FILE)-compressed.pdf $(FILE).pdf


# leaves pdf
clean:
	@ rm -f $(FILE).log $(FILE).aux $(FILE).toc $(FILE).blg $(FILE).bbl $(FILE).out $(FILE).fls $(FILE).fdb_latexmk $(FILE).glo $(FILE).lof $(FILE).lot $(FILE).nlg $(FILE).nlo $(FILE).nls

# leaves only the source
cleanall:
	@ rm -f $(FILE).log $(FILE).aux $(FILE).toc $(FILE).blg $(FILE).bbl $(FILE).out $(FILE).fls $(FILE).fdb_latexmk $(FILE).glo $(FILE).lof $(FILE).lot $(FILE).nlg $(FILE).nlo $(FILE).nls $(FILE).pdf $(FILE)-compressed.pdf
