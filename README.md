LATEX report Template
=====================

An example is provided with corresponding .tex, .sty and makefile. It
is not possible to compile the document since the graphics used are
not provided but the final output .pdf is included.
